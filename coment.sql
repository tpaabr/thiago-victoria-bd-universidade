/*CREATE TABLE secretaria (
	id SERIAL,
	nome VARCHAR(15) NOT NULL,
	PRIMARY KEY(id)
);
*/

/*
CREATE TABLE terceirizado(
	id SERIAL,
	salario INT NOT NULL,
	nome VARCHAR(30) NOT NULL,
	secretaria_id INT NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY (secretaria_id) REFERENCES secretaria(id)
);
*/

/*
CREATE TABLE departamento(
	id SERIAL,
	sigla VARCHAR(5) NOT NULL UNIQUE,
	secretaria_id INT NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY (secretaria_id) REFERENCES secretaria(id)
);
*/
/*
CREATE TABLE professor(
	id SERIAL,
	nome VARCHAR(30) NOT NULL,
	salario INT NOT NULL,
	email VARCHAR(30) NOT NULL UNIQUE,
	id_departamento INT NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY (id_departamento) REFERENCES departamento(id)
);
*/

/*
CREATE TABLE materia(
	id SERIAL,
	nome VARCHAR(30) NOT NULL,
	codigo CHAR(6) NOT NULL UNIQUE,
	id_departamento INT NOT NULL,
	FOREIGN KEY (id_departamento) REFERENCES departamento(id),
	PRIMARY KEY(id)
);
*/

/*
CREATE TABLE turma(
	id SERIAL,
	codigo CHAR(2),
	id_materia INT NOT NULL,
	id_professor INT NOT NULL,
	FOREIGN KEY(id_materia) REFERENCES materia(id),
	FOREIGN KEY(id_professor) REFERENCES professor(id),
	PRIMARY KEY(id)
);
*/

/*
CREATE TABLE aluno(
	id SERIAL,
	nome VARCHAR(30) NOT NULL,
	matricula CHAR(9) NOT NULL UNIQUE,
	curso VARCHAR(20) NOT NULL,
	PRIMARY KEY(id)
);
*/

/*
CREATE TABLE inscricao(
	id SERIAL,
	id_aluno INT NOT NULL,
	id_turma INT NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(id_aluno) REFERENCES aluno(id),
	FOREIGN KEY(id_turma) REFERENCES turma(id)
);
*/

/*
adcionando foreign key terceirizado, auto-relação
ERRO DIGITAÇÃO NOME COLUNA
ALTER TABLE terceirizado ADD id_superviso INT NOT NULL;
*/

/*
REMOVER ERRO DE DIGITAÇÃO 
ALTER TABLE terceirizado DROP COLUMN id_superviso;
*/

/*
COMETI O MESMO ERRO DE DIGITAÇÃO
ALTER TABLE terceirizado ADD id_superviso INT NOT NULL;
*/

/*
REMOVENDO ERRO COMETIDO PELA SEGUNDA VEZ
ALTER TABLE terceirizado DROP COLUMN id_superviso;
*/

/*
CONCERTADO O ERRO
ALTER TABLE terceirizado ADD id_supervisor INT NOT NULL;
*/

/*
ADCIONANDO FOREIGN KEY (AUTO-RELAÇÃO)
ALTER TABLE terceirizado ADD FOREIGN KEY (id_supervisor) REFERENCES terceirizado(id); 
*/

/*
RETIRANDO NOT NULL DA COLUNA id_supervisor
ALTER TABLE terceirizado DROP COLUMN id_supervisor;
*/

/*
preenchendo tabelas
INSERT INTO secretaria(nome) VALUES('secretaria_1');
INSERT INTO terceirizado(salario, nome, secretaria_id) VALUES(2000, 'Gilberto Silva', 1);
INSERT INTO terceirizado(salario, nome, secretaria_id, id_supervisor) VALUES(1000, 'Kaique Pontes', 1, 1);
INSERT INTO terceirizado(salario, nome, secretaria_id, id_supervisor) VALUES(1000, 'Joao Azevedo', 1, 1);
INSERT INTO inscricao(id_aluno, id_turma) VALUES(1, 1);
....
*/






